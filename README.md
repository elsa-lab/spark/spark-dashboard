# SPARK dashboard

For more information about the SPARK (the Supportive Personalized Adaptive Resource Kit), see the documentation at https://spark.opens.science.

## Usage

Production build
```cli
npm run start
```

Running local (on localhost)
```cli
npm run dev
```

Running local (on 10.0.2.2)
```cli
npm run app
```
