import React, { useState, useEffect } from "react"
import {
  Box,
  Icon,
  useMediaQuery,
  useTheme,
  Drawer,
  BottomNavigationAction,
  IconButton,
  Typography,
  ClickAwayListener,
  makeStyles,
  Theme,
  createStyles,
  withStyles,
  Tooltip,
  Popper,
  colors,
} from "@material-ui/core"
import { ReactComponent as Feed } from "../icons/Feed.svg"
import { ReactComponent as Learn } from "../icons/Learn.svg"
import { ReactComponent as Assess } from "../icons/Assess.svg"
import { ReactComponent as Manage } from "../icons/Manage.svg"
import { ReactComponent as PreventIcon } from "../icons/Prevent.svg"
import { useTranslation } from "react-i18next"
import LAMP from "lamp-core"
import Participant from "./Participant"
import schema from "../adaptable/theme.json"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    navigation: {
      "& svg": { width: 36, height: 36, padding: 6, borderRadius: "50%", opacity: 0.5 },
      [theme.breakpoints.up("md")]: {
        flex: "none",
        minHeight: 125,
      },
      width: "100%",
      minWidth: 64,
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: "rgba(255, 255, 255, 0.75)",
    },
    navigationFeedSelected: {
      "& div": {
        transform: "rotate(45deg)",
        backgroundImage:
          // `linear-gradient(45deg, #FFD645, #FFD645 100%), linear-gradient(135deg, #65DEB4, #65DEB4), linear-gradient(225deg, #FE8470, #FE8470) , linear-gradient(225deg, #7DB2FF, #7DB2FF)`,
          `linear-gradient(45deg, ${schema.palette.themeColor1.main}, ${schema.palette.themeColor1.main} 100%), linear-gradient(135deg, ${schema.palette.themeColor3.main}, ${schema.palette.themeColor3.main}), linear-gradient(225deg, ${schema.palette.themeColor4.main}, ${schema.palette.themeColor4.main}) , linear-gradient(225deg, ${schema.palette.themeColor2.main}, ${schema.palette.themeColor2.main})`,
        backgroundSize: "50% 50%",
        backgroundPosition: "0% 0%, 0% 100%, 100% 0%, 100% 100%",
        backgroundRepeat: "no-repeat",
        borderRadius: "50%",
        opacity: "1",
        width: 36,
        height: 36,
        "& svg": { opacity: 1 },
      },
      "& svg": {
        transform: "rotate(-45deg)",
      },
      "& span": { color: "black" },
      width: "100%",
    },
    navigationLearnSelected: {
      "& svg": {
        background: `${schema.palette.themeColor1.main} !important`,
        opacity: 1,
      },
      "& span": { color: "black" },
      width: "100%",
    },
    navigationManageSelected: {
      "& svg": {
        background: `${schema.palette.themeColor3.main} !important`,
        opacity: 1,
      },
      "& span": { color: "black" },
      width: "100%",
    },
    navigationAssessSelected: {
      "& svg": {
        background: `${schema.palette.themeColor2.main} !important`,
        opacity: 1,
      },
      "& span": { color: "black" },
      width: "100%",
    },
    navigationPreventSelected: {
      "& svg": {
        background: `${schema.palette.themeColor4.main} !important`,
        opacity: 1,
      },
      "& span": { color: "black" },
      width: "100%",
    },
    navigationLabel: {
      textTransform: "capitalize",
      fontSize: "12px !important",
      letterSpacing: 0,
      color: "rgba(0, 0, 0, 0.4)",
      width: "100%",
    },
    leftbar: {
      "& > div": {
        [theme.breakpoints.up("md")]: {
          backgroundColor: "#F8F8F8",
          border: 0,
        },
        "& a": {
          [theme.breakpoints.down("sm")]: {
            flex: 1,
          },
        },
      },
    },
    logResearcher: {
      "& > div": {
        marginTop: 50,
        height: "calc(100vh - 55px)",
        borderLeft: "#7599FF solid 5px",
        zIndex: 1111,
        [theme.breakpoints.down("sm")]: {
          borderBottom: "#7599FF solid 5px",
          borderRight: "#7599FF solid 5px",
        },
      },
    },
    btnCursor: {
      "&:hover span ": {
        cursor: "pointer !important",
      },
      "&:hover span > div": {
        cursor: "pointer !important",
      },
      "&:hover span > div > svg": {
        cursor: "pointer !important",
      },
      "&:hover span > div > svg > path": {
        cursor: "pointer !important",
      },
      "&:hover span > svg": {
        cursor: "pointer !important",
      },
      "&:hover span > svg > path": {
        cursor: "pointer !important",
      },
    },
    assessH: {
      // TODO: (tooltip background) hier iets mee
      background: "#E7F8F2 !important",
    },
    learnH: {
      background: "#FFF9E5 !important",
    },
    manageH: {
      background: "#FFEFEC !important",
    },
    preventH: {
      background: "#ECF4FF !important",
    },
  })
)

// FIXME: Determine colors based on tab background color
const GenericTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    zIndex: 999,
    padding: "25px 20px",
    boxShadow: "none",
    background: schema.palette.primary,
    borderRadius: 10,
    maxWidth: 345,
    right: 10,
    "& h6": { color: "white", fontWeight: 300, fontSize: 16, "& span": { fontWeight: 500 } },
    "& p": { color: "white", fontWeight: 300, marginTop: 10 },
    "& svg": { color: "white" },
    [theme.breakpoints.up("md")]: {
      right: 0,
    },
  },
  arrow: {
    color: schema.palette.primary,
    fontSize: 15,
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px !important",
    },
    [theme.breakpoints.down("xs")]: {
      marginLeft: "19px !important",
    },
  },
}))(Tooltip)

export async function sensorEventUpdate(val: string, participantId: string, activityId: string, timestamp?: number) {
  if (LAMP.Auth._type === "participant") {
    return await LAMP.SensorEvent.create(participantId, {
      timestamp: timestamp ?? new Date().getTime(),
      sensor: "lamp.analytics",
      data: {
        type: "open_page",
        page: val,
        activity: activityId,
      },
    })
  }
}

export default function BottomMenu({ ...props }) {
  const classes = useStyles()
  const supportsSidebar = useMediaQuery(useTheme().breakpoints.up("md"))
  const { t } = useTranslation()
  const tabs = [
    // TODO: Set this based on template config
    {
      id: 0,
      name: "feed",
      label: "Feed",
      classes: {
        root: classes.navigation,
        selected: classes.navigationFeedSelected,
        label: classes.navigationLabel,
      },
      icon: (
        <Box>
          <Feed />
        </Box>
      ),
      tooltip: (
        <React.Fragment>
          <IconButton aria-label="close" className={classes.closeButton} onClick={() => updateLocalStorage(0)}>
            <Icon>close</Icon>
          </IconButton>
          <Typography variant="h6">{`${t("Welcome to the Feed section")}`}</Typography>
          <Typography variant="body1">{`${t("Review today's activities.")}`}</Typography>
        </React.Fragment>
      ),
    },
    {
      id: 1,
      name: "learn",
      label: "Learn",
      classes: {
        root: classes.navigation,
        selected: classes.navigationLearnSelected,
        label: classes.navigationLabel,
      },
      icon: <Learn />,
      tooltip: (
        <React.Fragment>
          <IconButton aria-label="close" className={classes.closeButton} onClick={() => updateLocalStorage(1)}>
            <Icon>close</Icon>
          </IconButton>
          <Typography variant="h6">{`${t("Welcome to the Learn section")}`}</Typography>
          <Typography variant="body1">{`${t("Find useful information and practice healthy habits.")}`}</Typography>
        </React.Fragment>
      ),
    },
    {
      id: 2,
      name: "assess",
      label: "Assess",
      classes: {
        root: classes.navigation,
        selected: classes.navigationAssessSelected,
        label: classes.navigationLabel,
      },
      icon: <Assess />,
      tooltip: (
        <React.Fragment>
          <IconButton aria-label="close" className={classes.closeButton} onClick={() => updateLocalStorage(2)}>
            <Icon>close</Icon>
          </IconButton>
          <Typography variant="h6">{`${t("Welcome to the Assess section")}`}</Typography>
          <Typography variant="body1">{`${t("Log feelings, behavior, and activity.")}`}</Typography>
        </React.Fragment>
      ),
    },
    {
      id: 3,
      name: "manage",
      label: "Manage",
      classes: {
        root: classes.navigation,
        selected: classes.navigationManageSelected,
        label: classes.navigationLabel,
      },
      icon: <Manage />,
      tooltip: (
        <React.Fragment>
          <IconButton aria-label="close" className={classes.closeButton} onClick={() => updateLocalStorage(3)}>
            <Icon>close</Icon>
          </IconButton>
          <Typography variant="h6">{`${t("Welcome to the Manage section")}`}</Typography>
          <Typography variant="body1">{`${t("Take steps to refocus, reflect, and recover.")}`}</Typography>
        </React.Fragment>
      ),
    },
    {
      id: 4,
      name: "portal",
      label: "Portal",
      classes: {
        root: classes.navigation,
        selected: classes.navigationPreventSelected,
        label: classes.navigationLabel,
      },
      icon: <PreventIcon />,
      tooltip: (
        <React.Fragment>
          <IconButton aria-label="close" className={classes.closeButton} onClick={() => updateLocalStorage(4)}>
            <Icon>close</Icon>
          </IconButton>
          <Typography variant="h6">{`${t("Welcome to the Portal section")}`}</Typography>
          <Typography variant="body1">{`${t("Track progress and make connections.")}`}</Typography>
        </React.Fragment>
      ),
    },
  ]

  const getTab = (tab: string) => {
    return tabs.find((item) => item.name === tab).id
  }
  const [openTab, _setTab] = useState(getTab(props.tabValue))
  const [tabValues, setTabValues] = useState(
    localStorage.getItem("bottom-menu-tabs" + props.participant.id) !== null
      ? JSON.parse(localStorage.getItem("bottom-menu-tabs" + props.participant.id))
      : []
  )

  useEffect(() => {
    sensorEventUpdate(tabs[openTab].name, props.participant.id, null)
    props.activeTab(tabs[openTab].name, props.participant.id)
  }, [])

  const openTabUpdate = (val) => {
    sensorEventUpdate(tabs[val].name, props.participant.id, null)
    props.activeTab(tabs[val].name, props.participant.id)
    _setTab(val)
    props.setShowDemoMessage(false)
  }

  const updateLocalStorage = (tab) => {
    setTabValues({ ...tabValues, [tab]: false })
  }

  useEffect(() => {
    localStorage.setItem("bottom-menu-tabs" + props.participant.id, JSON.stringify(tabValues))
  }, [tabValues])

  return (
    <div>
      <Box clone displayPrint="none">
        <Drawer
          open
          className={classes.leftbar + (LAMP.Auth._type === "participant" ? "" : " " + classes.logResearcher)}
          anchor={supportsSidebar ? "left" : "bottom"}
          variant="permanent"
          PaperProps={{
            style: {
              flexDirection: supportsSidebar ? "column" : "row",
              justifyContent: !supportsSidebar ? "center" : undefined,
              alignItems: "center",
              height: !supportsSidebar ? 80 : undefined,
              width: supportsSidebar ? 100 : undefined,
              transition: "all 500ms ease-in-out",
            },
          }}
        >
          {(tabs ?? []).map((_, index) => (
            <GenericTooltip
              open={openTab == index && (typeof tabValues[index] === "undefined" || !!tabValues[index])}
              interactive={true}
              className={classes.btnCursor}
              title={tabs[openTab].tooltip}
              arrow={true}
              placement={supportsSidebar ? "right" : "top"}
            >
              <BottomNavigationAction
                showLabel
                selected={openTab === index}
                label={`${t(tabs[index].label)}`}
                value={index}
                classes={tabs[index].classes}
                icon={tabs[index].icon}
                onChange={(_, newTab) => openTabUpdate(newTab)}
              />
            </GenericTooltip>
          ))}
        </Drawer>
      </Box>
    </div>
  )
}
