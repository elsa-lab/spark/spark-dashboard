import React, { useState } from "react"
import {
  Box,
  Icon,
  Button,
  Fab,
  Dialog,
  DialogContent,
  DialogActions,
  makeStyles,
  Theme,
  createStyles,
} from "@material-ui/core"
import { useSnackbar } from "notistack"
import LAMP from "lamp-core"
import { useTranslation } from "react-i18next"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    disabledButton: {
      color: "#4C66D6 !important",
      opacity: 0.5,
    },
    activityContent: {
      padding: "25px 50px 0",
    },
    manageStudyDialog: { maxWidth: 700 },
    btnWhite: {
      background: "#008000",
      borderRadius: "40px",
      boxShadow: "none",
      cursor: "pointer",
      textTransform: "capitalize",
      fontSize: "14px",
      "&:hover": { background: "#009a00", boxShadow: "0px 3px 5px rgba(0, 0, 0, 0.20)" },
    },
    btnBlack: {
      background: "#ff0000",
      borderRadius: "40px",
      boxShadow: "none",
      cursor: "pointer",
      textTransform: "capitalize",
      fontSize: "14px",
      "&:hover": { background: "#ff3232", boxShadow: "0px 3px 5px rgba(0, 0, 0, 0.20)" },
    },
  })
)

export default function JoinableStudy({ study, deletedStudy, researcherId, ...props }) {
  const { enqueueSnackbar } = useSnackbar()
  const classes = useStyles()
  const { t } = useTranslation()
  const [openDialogJoinableStudy, setOpenDialogJoinableStudy] = useState(false)
  const [studyIdJoinable, setStudyIdForJoinable] = useState("")
  const [isJoinable, setIsJoinable] = useState(study.joinable)

  const handleJoinableStudy = async (studyId: string) => {
    setOpenDialogJoinableStudy(false)
    let body = { joinable: !isJoinable }
    const baseUrl =
      (process.env.REACT_APP_ENV === "production" ? "https" : "http") + "://" + process.env.REACT_APP_API_URL
    fetch(baseUrl + `/study/${studyId}/toggle`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Basic ${LAMP.Auth._auth.id}:${LAMP.Auth._auth.password}`,
      },
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then(
        (res) => {
          setIsJoinable(res.data.joinable)
        },
        (err) => {
          enqueueSnackbar(
            `${t("Failed setting study: errorMessage", {
              alias: study,
              errorMessage: err.message,
            })}`,
            { variant: "error" }
          )
        }
      )
  }

  const handleCloseJoinableStudy = () => {
    setOpenDialogJoinableStudy(false)
  }

  return (
    <React.Fragment>
      <Box display="flex" alignItems="center" pl={1}>
        <Fab
          size="small"
          color="primary"
          disabled={study.id > 1 ? true : false}
          classes={{ root: isJoinable ? classes.btnWhite : classes.btnBlack, disabled: classes.disabledButton }}
          onClick={() => {
            setOpenDialogJoinableStudy(true)
            setStudyIdForJoinable(study.id)
          }}
        >
          {isJoinable ? (
            <Icon style={{ color: "white" }}>meeting_room</Icon>
          ) : (
            <Icon style={{ color: "white" }}>no_meeting_room</Icon>
          )}
        </Fab>
      </Box>

      <Dialog
        open={openDialogJoinableStudy}
        onClose={handleCloseJoinableStudy}
        scroll="paper"
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
        classes={{ paper: classes.manageStudyDialog }}
      >
        <DialogContent dividers={false} classes={{ root: classes.activityContent }}>
          <Box mt={2} mb={2}>
            {isJoinable
              ? `${t("Are you sure you want to allow participants to join this study?")}`
              : `${t("Are you sure you want to stop participants from joining this study?")}`}
          </Box>
          <DialogActions>
            <Box textAlign="center" width={1} mb={3}>
              <Button onClick={() => handleJoinableStudy(studyIdJoinable)} color="primary" autoFocus>
                {`${t("Yes")}`}
              </Button>

              <Button
                onClick={() => {
                  handleCloseJoinableStudy()
                }}
              >
                {`${t("Cancel")}`}
              </Button>
            </Box>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  )
}
