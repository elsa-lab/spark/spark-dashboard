import React, { useEffect } from "react"
import ToggleButton from "@material-ui/lab/ToggleButton"
import { makeStyles, Theme, createStyles } from "@material-ui/core"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    togglebtn: {
      padding: "5px 15px",
      borderRadius: "30px",
      color: "#7599FF !important",
      fontSize: "13px",
      textTransform: "capitalize",
      background: "#fff !important",
      fontWeight: 600,
    },
  })
)

export default function ModeToggleButton({ changeResearcherType, ...props }) {
  const [selected, setSelected] = React.useState(0)
  const classes = useStyles()

  const modes = [
    { type: "clinician", name: "Simple Mode" },
    { type: "researcher", name: "Advanced Mode" },
    { type: "other", name: "AAA Mode" },
  ]

  useEffect(() => {
    const mode = localStorage.getItem("mode") !== null ? localStorage.getItem("mode") : "clinician"
    setSelected(modes.findIndex((item) => item.type === mode))
    changeResearcherType(mode)
  }, [])

  useEffect(() => {
    const mode = modes[selected].type
    changeResearcherType(mode)
    localStorage.setItem("mode", mode)
  }, [selected])

  return (
    <ToggleButton
      className={classes.togglebtn}
      value="check"
      selected={true}
      onChange={() => {
        setSelected((selected + 1) % 3)
      }}
    >
      {modes[selected].name}
    </ToggleButton>
  )
}
