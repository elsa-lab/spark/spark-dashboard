import React, { useState } from "react"
import {
  Box,
  DialogContent,
  makeStyles,
  Theme,
  createStyles,
  Fab,
  Icon,
  Dialog,
  DialogActions,
  TextField,
  Button,
} from "@material-ui/core"
import LAMP, { Researcher } from "lamp-core"
import { useSnackbar } from "notistack"
import { useTranslation } from "react-i18next"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    header: {
      "& h5": {
        fontSize: "30px",
        fontWeight: "bold",
      },
    },
    optionsMain: {
      background: "#ECF4FF",
      borderTop: "1px solid #C7C7C7",
      marginTop: 20,
      width: "99.4vw",
      position: "relative",
      left: "50%",
      right: "50%",
      marginLeft: "-50vw",
      marginRight: "-50vw",
    },
    optionsSub: { width: 1030, maxWidth: "80%", margin: "0 auto", padding: "10px 0" },
    btnWhite: {
      background: "#fff",
      borderRadius: "40px",
      boxShadow: "none",
      cursor: "pointer",
      textTransform: "capitalize",
      fontSize: "14px",
      color: "#7599FF",
      "& svg": { marginRight: 8 },
      "&:hover": { color: "#5680f9", background: "#fff", boxShadow: "0px 3px 5px rgba(0, 0, 0, 0.20)" },
    },
    btnBlue: {
      background: "#7599FF",
      borderRadius: "40px",
      minWidth: 100,
      boxShadow: "0px 3px 5px rgba(0, 0, 0, 0.20)",
      lineHeight: "38px",
      cursor: "pointer",
      textTransform: "capitalize",
      fontSize: "16px",
      color: "#fff",
      "& svg": { marginRight: 8 },
      "&:hover": { background: "#5680f9" },
      [theme.breakpoints.up("md")]: {
        position: "absolute",
        top: 0,
      },
      [theme.breakpoints.down("sm")]: {
        minWidth: "auto",
      },
    },
  })
)
export default function AddUpdateOrganization({
  organization,
  organizations,
  refreshOrganizations,
  setName,
  updateStore,
  ...props
}: {
  organization?: any
  organizations?: any
  refreshOrganizations?: Function
  setName?: Function
  updateStore?: Function
}) {
  const classes = useStyles()
  const { enqueueSnackbar } = useSnackbar()
  const { t } = useTranslation()
  const [open, setOpen] = useState(false)
  const [name, setOrganizationName] = useState(!!organization ? organization.name : "")
  const [rData, setRdara] = useState(organization)

  const addOrganization = async () => {
    let duplicates = organizations.filter((x) =>
      !!organization
        ? x.name?.toLowerCase() === name?.trim().toLowerCase() && x.id !== organization?.id
        : x.name?.toLowerCase() === name?.trim().toLowerCase()
    )
    if (duplicates.length > 0) {
      enqueueSnackbar(`${t("Organization with same name already exists.")}`, {
        variant: "error",
      })
      setOrganizationName(!!organization ? organization.name : "")
    } else {
      // FIXME: do this using patched lamp sdk

      const organizationObj = {
        name: name,
        parentOrganization: null, // TODO: allow choosing parent organization in UI
      }

      try {
        // TODO: encapsulate with updated lamp sdk
        let headers = new Headers()
        headers.set("Authorization", "Basic " + btoa(LAMP.Auth._auth.id + ":" + LAMP.Auth._auth.password))
        headers.set("Content-Type", "application/json")
        const url =
          (process.env.REACT_APP_ENV === "production" ? "https" : "http") +
          "://" +
          process.env.REACT_APP_API_URL +
          "/organization" +
          (!!organization ? `/${organization.id}` : "")
        fetch(url, {
          method: !!organization ? "PUT" : "POST",
          headers: headers,
          body: JSON.stringify(organizationObj),
        })
          .then((response) => response.json())
          .then((body) => {
            let data = body.data
            if (!!organization) {
              updateStore(organization.id)
              setName(name.trim())
              setRdara({ ...rData, name: name.trim() })
            } else {
              setOrganizationName("")
              refreshOrganizations()
            }
            enqueueSnackbar(
              !!organization
                ? `${t("Successfully updated an organization.")}`
                : `${t("Successfully created a new organization.")}`,
              {
                variant: "success",
              }
            )
            setOpen(false)
          })
      } catch (error) {
        enqueueSnackbar(`${t("Failed to create a new organization.")}`, {
          variant: "error",
        })
      }
    }
  }

  return (
    <Box display="flex" alignItems="start">
      {!!organization ? (
        <Fab size="small" classes={{ root: classes.btnWhite }} onClick={() => setOpen(true)}>
          <Icon>edit</Icon>
        </Fab>
      ) : (
        <Fab variant="extended" classes={{ root: classes.btnBlue }} onClick={() => setOpen(true)}>
          <Icon>add</Icon> {`${t("Add")}`}
        </Fab>
      )}
      <Dialog open={open} onClose={() => setOpen(false)} aria-labelledby="form-dialog-title">
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={`${t("Name")}`}
            fullWidth
            onChange={(event) => setOrganizationName(event.target.value)}
            value={name}
            helperText={
              typeof name == "undefined" || name === null || name.trim() === "" ? `${t("Please enter name")}` : ""
            }
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              setOpen(false)
              setOrganizationName(!!rData ? rData.name : "")
            }}
            color="primary"
          >
            {`${t("Cancel")}`}
          </Button>
          <Button
            onClick={() => addOrganization()}
            color="primary"
            disabled={typeof name == "undefined" || name === null || name.trim() === "" ? true : false}
          >
            {!!organization ? `${t("Update")}` : `${t("Add")}`}
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  )
}
